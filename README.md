
# Asumsi

   Hal ini dikarenakan tidak adanya validasi ketika stock sudah habis, dalam kasus ini kemungkinan disebabkan oleh : jumlah checkout (menunggu proses pembayaran) yang (jauh) lebih banyak dari stock available.  Sehingga saat semua transaksi sudah terbayar, jumlah stock tidak mencukupi untuk dikirimkan.


# Solusi

   Solusi yang diajukan sebisa mungkin untuk memperbaiki user experience, dengan mengurangi kemungkinan timbulnya ketidak nyamanan dari sisi user. Dari sisi aplikasi hal ini dapat dicapai dengan menambahkan validasi :


1. Jumlah checkout tidak boleh melebihi dari stock. [DONE]
2. ATAU jumlah checkout boleh melebihi jumlah stock, dengan buffer kapasitas onhold sekian persen. [DONE]
3. Memberi warning kepada user jika jumlah stock (setelah dikurangi transaksi berhasil bayar) tinggal sedikit. [NOT_DONE]
4. Untuk menghindari pembayaran oleh user, batalkan transaksi checkout yang belum bayar,  jika jumlah stock sudah habis (atau tinggal sekian persen). [NOT_DONE]

# Menjalankan Aplikasi
* Clone project ini, lalu masuk ke folder tersebut
```
git clone https://adie-id@bitbucket.org/adie-id/evermos.git
cd evermos
```
* Create database di mysql
* Set db config di .env sesuai dengan database yang baru dibuat
* Di folder aplikasi, migrate database. Ini akan meng-create table, dan mengisi dengan data stock dummy
```
   php artisan migrate --seed
```
* Jalankan server
```
php artisan serve
```
* API dapat diakses di http://localhost:8000/api/ 
* postman API collection ada di folder others/evermos.postman_collection.json
