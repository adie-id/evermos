<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::get('/stock', 'App\Http\Controllers\StockController@index');
Route::post('/stock', 'App\Http\Controllers\StockController@store');

Route::get('/transaction', 'App\Http\Controllers\TransactionController@index');
Route::post('/checkout', 'App\Http\Controllers\TransactionController@checkout');
Route::post('/payment', 'App\Http\Controllers\TransactionController@payment');
