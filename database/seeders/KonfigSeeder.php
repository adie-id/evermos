<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class KonfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('konfigs')->insert([            
            'param' => 'kapasitas_percentage',
            'value' => 1.5,
            'description' => 'jumlah kapasitas yang dapat ditampung di checkout' 
        ]);
    }
}
