<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class StockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stocks')->insert([
            'sku' => strtoupper(Str::random(10)),
            'stock_name' => 'Ansania - Hijab Ansania Byanca Series',
            'harga' => 35000,
            'stock_sisa' => 42
        ]);
        DB::table('stocks')->insert([
            'sku' => strtoupper(Str::random(10)),
            'stock_name' => 'Madu Azzura - Madu Murni Trigona 200 gr',
            'harga' => 90250,
            'stock_sisa' => 10
        ]);
        DB::table('stocks')->insert([
            'sku' => strtoupper(Str::random(10)),
            'stock_name' => 'Arra - Al Faruq Koko Pakistan Dewasa Navy',
            'harga' => 185000,
            'stock_sisa' => 12
        ]);
    }
}
