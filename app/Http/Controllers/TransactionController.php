<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\Stock;
use App\Models\Konfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Transaction::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function checkout(Request $request)
    {
        DB::beginTransaction();

        try {            
            $t = new Transaction;
            $t->stock_id = $request->stock_id;
            $t->jumlah = $request->jumlah;
            $t->save();

            $k = Konfig::where('param', 'max_onhold')->first();
            $s = Stock::find($t->stock_id);
            $kapasitas = $s->stock_sisa * $k->value;
            if ($kapasitas - $s->stock_onhold > $request->jumlah) {
                //check jika stock dikurangi onhold cukup
                $s->stock_onhold += $request->jumlah;
                $s->save();
            } else {
                DB::rollback();
                return response()->json(['error' => 'Stock sisa (yang tidak onhold) kurang dari jumlah dipesan'], 500);
            }

            DB::commit();
            return response()->json(['success' => 'Record creation success'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => 'Record creation failed'], 500);
        }
    }

    public function payment(Request $request)
    {
        DB::beginTransaction();

        try {
            $t = Transaction::find($request->transaction_id);
            $s = Stock::find($t->stock_id);

            if ($s->stock_sisa > $t->jumlah) {
                //check jika stock_sisa cukup
                $t->status = 'PAID';
                $t->save();

                $s->stock_sisa -= $t->jumlah;
                $s->stock_onhold -= $t->jumlah;
                $s->save();
            } else {
                DB::rollback();
                return response()->json(['error' => 'Payment failed. Stock kurang'], 500);
            }
            DB::commit();
            return response()->json(['success' => 'Payment success dan stock cukup'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => 'Payement failed'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
