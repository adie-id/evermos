<?php

namespace App\Http\Controllers;

use App\Models\Konfig;
use Illuminate\Http\Request;

class KonfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Konfig  $konfig
     * @return \Illuminate\Http\Response
     */
    public function show(Konfig $konfig)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Konfig  $konfig
     * @return \Illuminate\Http\Response
     */
    public function edit(Konfig $konfig)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Konfig  $konfig
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Konfig $konfig)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Konfig  $konfig
     * @return \Illuminate\Http\Response
     */
    public function destroy(Konfig $konfig)
    {
        //
    }
}
